#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

from django.http import HttpResponse
from django.conf import settings
#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from login.OAuth import *

from rest_framework.exceptions import PermissionDenied

import os, shutil
import shutil
from zipfile import ZipFile
from os import path
from django.http import HttpResponse

real_dir = os.getcwd()
class Examples(viewsets.ModelViewSet):
    serializer_class = ExampleSerializer
    queryset = Example.objects.all()

    def list(self, request, *args, **kwargs):
        serializer = ExampleSerializer(self.get_queryset(), many=True)
        print(request.session.get("authorization"))
        if is_authenticated(request):
            return Response({})
        else:
            return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        file = self.request.FILES.get('upload')
        if is_authenticated(request):
            if serializer.is_valid() :
                shutil.rmtree(real_dir+'/Files')
                examples = Example.objects.create(upload = file)
                file_name = str(examples.upload).split("/")[1]
                os.chdir(real_dir+'/Files')
                ZipFile(file_name+'.zip', mode='w').write(file_name, arcname = file_name)
                zip_file = open(file_name+ ".zip", 'rb')
                response = HttpResponse(zip_file, content_type='application/zip')
                response['Content-Disposition'] = 'attachment; filename='+ file_name + ".zip"
                os.chdir(real_dir)
                return response
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Message": "silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)